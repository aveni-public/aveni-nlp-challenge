# Aveni-NLP-Challenge


At Aveni, our aim is to assist our customers to extract value from the natural language interactions with their customers. Understanding those interactions and extracting relevant information from them is a crucial part of our work.

In this exercise, we will provide a dataset that resembles the type of data we manage at Aveni. The dataset is a collection of transcripts of customer-agent oral interactions, which contains a variety of different types of conversations. 



Given that dataset, we propose two different tasks:

### **Task 1: Topic Classification**

Is it possible to identify the main topics of the conversations in the dataset? Which will be the best method for achieving this? Which will be the best way to evaluate such a method?
Is there any external dataset you can use to help you with this task?
Given that set of topics, can you automatically classify each call by topic? Please provide an implementation in Python for classifying a set of calls by topic. 


### **Task 2: Call Segmentation**

Is it possible to identify frequent segments in the calls? Which will be the best method for achieving this? Which will be the best way to evaluate such a method?
Is there any external dataset you can use to help you with this task?
Can you automatically segment calls in the dataset into those relevant parts? Please provide an implementation in Python with your solution and your results. 




## Output of the exercise
You can use external tools, libraries or ideas from research/products you know. You should present your findings and solutions as a Jupyter Notebook, containing some notes, analysis of the data and your solution to the problem. It is important that you explain the reasoning behind your data analysis and your proposed solution to the problem. 
This task is meant to be unstructured and open-ended, so there is no single correct solution. We are not looking for a written report or a perfect coding exercise, we are more interested in the way you tackle the problem and apply your NLP skills to devise a solution. We will discuss your analysis in the follow-up interview. 
